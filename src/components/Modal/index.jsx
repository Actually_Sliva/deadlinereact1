import style from './modal.module.scss'
import { Component } from 'react'

export default class Modal extends Component {

    render() {
        const { id, title, description, closeBtn, closeBtnHandler, actions } = this.props.data
        const { onClick } = this.props

        return (
            <div className={style.modal + ' ' + style[id]} onClick={onClick}>
                <div className={style.modal__content} onClick={(e) => e.stopPropagation()}>
                    <header className={style.modal__header}>
                        <h3 className={style.modal__title}>
                            {title}
                        </h3>
                        {closeBtn && closeBtnHandler(onClick, style.close)}
                    </header>
                    <div className={style.modal__body}>
                        {description && <p>{description}</p>}
                        {actions && actions(onClick, onClick, style.modal__btns)}
                    </div>
                </div>
            </div>
        )
    }
}